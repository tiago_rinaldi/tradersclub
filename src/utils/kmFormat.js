const kmFormat = (x) => {
  if(x !== null || x !== undefined || x !== "0" || x !== "") {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".") + " KM";
  }
}

export default kmFormat;