import React, { Fragment } from "react";
import Sidebar from "../SideBar/"
import TopBar from "../TopBar/";

import "../../assets/styles/Shared.css";



const App = ({
  loading,
  isOnline
}) => (
  <Fragment>
    <div className="app">
    <TopBar />
    <Sidebar />
    </div>
    {!loading && !isOnline && <h1>Ops, a API esta OFFLINE</h1>}
  </Fragment>
);

export default App;
