import App from "./App";
import { lifecycle, compose } from "recompose";

const isApiOnline = lifecycle({
  state: { loading: true, isOnline: false },
  componentDidMount() {
    fetch("http://tchml.tradersclub.com.br:12000/api/")
    //fetch("https://private-anon-7be4f2a24b-tradersclubapi.apiary-mock.com/api/")
    .then(res => res.json())
    .then((data) => {
      this.setState({ loading: false, isOnline: data.online })
    })
  }
});

const enhance = compose(
  isApiOnline,
)

export default enhance(App);
