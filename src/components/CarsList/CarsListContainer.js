import CarList from "./CarsList";
import { withState, compose } from "recompose";

const enhance = compose(
  withState("carId", "setCarId", false),
);

export default enhance(CarList);