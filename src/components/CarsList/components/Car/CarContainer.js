import Car from "./Car";
import { compose } from "recompose";

const enhance = compose();

export default enhance(Car);