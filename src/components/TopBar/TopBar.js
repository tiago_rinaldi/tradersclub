import React, { Fragment } from "react";
import CarsList from "../CarsList/";
import Details from "../Details";

const TopBar = ({
  search,
  onSearchCars,
  register,
  setRegister
}) => (
  <Fragment>
  <header>
    <div className="top-form">
    <input
      type="text"
      name="cars" 
      placeholder="Pesquise por um veículo"
      className="search"
      onChange={(e) => {onSearchCars(e.target.value)}}
    />
    <input
      type="button"
      name="register"
      value="Cadastrar"
      className="btn active"
      onClick={() => setRegister(!register)}
    />
    </div>
    <article className="results">
      {
        !search && register && <Details  />
      }
      {search && search.cars && (
        () => setRegister(!register),
        <CarsList cars={search.cars} />
      )}
      {!register 
      && !search 
      && !search.cars
      && (
      <section className="banner">
        Pesquisa de veículos <br /> do TradersClub
      </section>
      )}
    </article>

  </header>

  </Fragment>
);

export default TopBar;